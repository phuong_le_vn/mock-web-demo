/* eslint-disable no-unused-vars */
/* eslint-disable global-require */
// [START functionsimport]
const functions = require('firebase-functions');
const express = require('express');
// const cors = require('cors');
const request = require('request');

const matchMock = require('./matchMock');

const app = express();

app.use(matchMock);

// Automatically allow cross-origin requests
// app.use(cors({ origin: true }));

const handleRedirect = async (req, res) => {
    console.log("Hello from Firebase!");
    const { userName, password, type } = req.body; 
    const options = {
        method: 'POST',
        url: 'https://ami-operation-portal-dev.public.tmn-dev.com/ami-operation-portal/api/account/login',
        // headers: req.headers,
        form: {
            username: userName,
            password,
            type
        }
    };
    console.log(`Options: ${JSON.stringify(options)}`);
    console.log(`Request: ${JSON.stringify(req.body)}`);
    console.log(`Headers: ${JSON.stringify(req.headers)}`);
    try {
        await request.post(options, (error, response, body) => {
            console.log(`Response: ${JSON.stringify(response)}`);
            console.log(`Error: ${JSON.stringify(error)}`);
            console.log(`Body: ${JSON.stringify(body)}`);
            if (!error && response.statusCode === 200) {
                res.status(200).send(body);
            } else {
                res.status(response.statusCode).send(error);
            }
        });
    } catch (error) {
        console.error(error);
        res.status(500).send('Something went wrong while login EQ System.');
    }

    // res.end();
}
app.post('/api/account/login', handleRedirect);

exports.api = functions.https.onRequest(app);