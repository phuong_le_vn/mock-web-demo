import React, { Fragment } from 'react';
import { formatMessage } from 'umi/locale';
import Link from 'umi/link';
import { Icon } from 'antd';
import { connect } from 'dva';
import GlobalFooter from '@/components/GlobalFooter';
import SelectLang from '@/components/SelectLang';
import styles from './UserLayout.less';
import logo from '../assets/logo.svg';

// const links = [
//   {
//     key: 'help',
//     title: formatMessage({ id: 'layout.user.link.help' }),
//     href: '',
//   },
//   {
//     key: 'privacy',
//     title: formatMessage({ id: 'layout.user.link.privacy' }),
//     href: '',
//   },
//   {
//     key: 'terms',
//     title: formatMessage({ id: 'layout.user.link.terms' }),
//     href: '',
//   },
// ];

// const copyright = (
//   <Fragment>
//     Copyright <Icon type="copyright" /> 2019 Taurus team
//   </Fragment>
// );

// const UserLayout = ({ children }) => (
//   // @TODO <DocumentTitle title={this.getPageTitle()}>
//   <div className={styles.container}>
//     <div className={styles.lang}>
//       <SelectLang />
//     </div>
//     <div className={styles.content}>
//       <div className={styles.top}>
//         <div className={styles.header}>
//           <Link to="/">
//             <img alt="logo" className={styles.logo} src={logo} />
//             <span className={styles.title}>Embedded Mock Web</span>
//           </Link>
//         </div>
//         <div className={styles.desc}>Website Demo</div>
//       </div>
//       {children}
//     </div>
//     <GlobalFooter links={links} copyright={copyright} />
//   </div>
// );

// export default UserLayout;

class UserLayout extends React.Component {
  componentDidMount() {
      window.addEventListener('message', this.onPostLogin);
  }

  componentWillUnmount() {
      window.removeEventListener('message', this.onPostLogin);
  }

  onPostLogin = event => {
    console.log('[Mock Web] Post message triggered');
    const { type, payload } = event.data;
    const { dispatch } = this.props;
    switch (type) {
      case 'login':
        console.log('[Mock Web] Message received for login: ', event.data);
        dispatch({
          type: 'login/login',
          payload: {
            userName: payload.username,
            password: payload.password,
            type: 'account',
            referrer: payload.referrer
          },
        });
        break;
      default:
        console.log('[Mock Web] Message received: ', event.data);
        break;
    }
  };

  render() {
    const { children } = this.props;
    const links = [
      {
        key: 'help',
        title: formatMessage({ id: 'layout.user.link.help' }),
        href: '',
      },
      {
        key: 'privacy',
        title: formatMessage({ id: 'layout.user.link.privacy' }),
        href: '',
      },
      {
        key: 'terms',
        title: formatMessage({ id: 'layout.user.link.terms' }),
        href: '',
      },
    ];

    const copyright = (
      <Fragment>
        Copyright <Icon type="copyright" /> 2019 Taurus team
      </Fragment>
    );

    return (
        // @TODO <DocumentTitle title={this.getPageTitle()}>
      <div className={styles.container}>
        <div className={styles.lang}>
          <SelectLang />
        </div>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
              <Link to="/">
                <img alt="logo" className={styles.logo} src={logo} />
                <span className={styles.title}>Embedded Mock Web</span>
              </Link>
            </div>
            <div className={styles.desc}>Website Demo</div>
          </div>
          {children}
        </div>
        <GlobalFooter links={links} copyright={copyright} />
      </div>
    );
  }
}

export default connect(() => ({
}))(props => (
  <UserLayout {...props} />
));
