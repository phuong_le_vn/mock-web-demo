import React from 'react';
import { connect } from 'dva';

// export default props => <div {...props} />;
class BlankLayout extends React.Component {
    componentDidMount() {
        window.addEventListener('message', this.onPostLogout);
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.onPostLogout);
    }

    onPostLogout = event => {
        console.log('[Mock Web] Post message triggered');
        const { type } = event.data;
        const { dispatch } = this.props;
        switch (type) {
          case 'logout':
            console.log('[Mock Web] Message received for logout: ', event.data);
            dispatch({
              type: 'login/logout',
            });
            break;
          default:
            console.log('[Mock Web] Message received: ', event.data);
            break;
        }
      };

    render() {
        return (<div {...this.props} />);
    }
}

export default connect(() => ({
  }))(props => (
    <BlankLayout {...props} />
  ));
